FROM node:alpine


RUN mkdir -p /app

ADD package-lock.json /tmp/
ADD package.json /tmp/
ADD yarn.lock /tmp/
RUN cd /tmp && yarn install

RUN mkdir -p /app && rm -rf /app/node_modules && cp -a /tmp/node_modules /app/


WORKDIR /app

CMD npm run-script start
