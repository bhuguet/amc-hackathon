import React, { createElement, Component } from 'react';
import PropTypes from 'prop-types';
import { render } from 'react-dom';

// redux, react-router, redux-form, saga, and material-ui
// form the 'kernel' on which admin-on-rest runs
import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import thunkMiddleware from 'redux-thunk'
import createHistory from 'history/createHashHistory';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import siteEditorReducer from './reducers/Publishers/SiteEditorReducer';
import { SiteList, SiteCreate } from './sites'
import SiteEditorContainer from './containers/Publishers/SiteEditorContainer'
import menu from './Menu'
import CircularProgress from 'material-ui/CircularProgress';
import customRoutes from './Routes'

// prebuilt admin-on-rest features
import {
    adminReducer,
    localeReducer,
    crudSaga,
    jsonServerRestClient,
    Delete,
    TranslationProvider,
    declareResources,
} from 'admin-on-rest';

import {
    AdminRoutes,
    AppBar,
    Menu,
    Notification,
    Sidebar,
    setSidebarVisibility,
} from 'admin-on-rest';

// your app components
import Dashboard from './Dashboard';
// create a Redux app
const reducer = combineReducers({
    admin: adminReducer,
    locale: localeReducer(),
    form: formReducer,
    routing: routerReducer,
    site: siteEditorReducer
});
const sagaMiddleware = createSagaMiddleware();
const history = createHistory();

const store = createStore(reducer, 
    undefined,
    compose(
    applyMiddleware(sagaMiddleware, thunkMiddleware, routerMiddleware(history)),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
));
store.dispatch(declareResources([{ name: 'sites' }]));
const restClient = jsonServerRestClient('http://localhost:3000/api/v2/publishers/BuySellAds%20(BSA)');
sagaMiddleware.run(crudSaga(restClient));


const styles = {
    wrapper: {
        // Avoid IE bug with Flexbox, see #467
        display: 'flex',
        flexDirection: 'column',
    },
    main: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
    },
    body: {
        backgroundColor: '#edecec',
        display: 'flex',
        flex: 1,
        overflowY: 'hidden',
        overflowX: 'scroll',
    },
    content: {
        flex: 1,
        padding: '2em',
    },
    loader: {
        position: 'absolute',
        top: 0,
        right: 0,
        margin: 16,
        zIndex: 1200,
    },
};


class App extends Component {
    componentWillMount() {
       // this.props.setSidebarVisibility(true);
    }

    render() {
        const {
            children,
            isLoading,
            logout,
        } = this.props;
        return <Provider store={store}>
                <TranslationProvider>
                    <ConnectedRouter history={history}>
                        <MuiThemeProvider>
                            <div style={styles.wrapper}>
                                <div style={styles.main}>
                                    <AppBar title="My Admin" />
                                    <div className="body" style={styles.body}>
                                        <div style={styles.content}>
                                            <AdminRoutes
                                                customRoutes={customRoutes}
                                                dashboard={Dashboard}
                                            >
                                                {children}
                                            </AdminRoutes>
                                        </div>
                                        <Sidebar>
                                            {createElement(menu || Menu, {
                                                logout,
                                                hasDashboard: !!Dashboard,
                                            })}
                                        </Sidebar>
                                    </div>
                                    <Notification />
                                    {isLoading && (
                                        <CircularProgress
                                            color="#fff"
                                            size={30}
                                            thickness={2}
                                            style={styles.loader}
                                        />
                                    )}
                                </div>
                            </div>
                        </MuiThemeProvider>
                    </ConnectedRouter>
                </TranslationProvider>
            </Provider>
    }
};

export default App;