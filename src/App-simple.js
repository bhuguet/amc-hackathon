import React from 'react';
import { jsonServerRestClient, Admin, Resource, Delete } from 'admin-on-rest';

// in src/App.js
//import { PostList, PostEdit, PostCreate } from './posts';
//import { UserList } from './users';
import { SiteList, SiteCreate } from './sites';
import SiteEditorContainer from './containers/Publishers/SiteEditorContainer'
import siteEditorReducer from './reducers/Publishers/SiteEditorReducer';
import MyLayout from './CustomLayout';

//rely on branch feature/ADP-3088-site_collection_unique_functional_id
const App = () => (
    <div>
        <Admin appLayout={MyLayout} customReducers={{ site: siteEditorReducer }} restClient={jsonServerRestClient('http://localhost:3000/api/v2/publishers/BuySellAds%20(BSA)')}>
            <Resource name="sites" list={SiteList} edit={SiteEditorContainer} create={SiteCreate} remove={Delete} />
        </Admin>
    </div>
);

export default App;
