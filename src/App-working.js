import React from 'react';
import PropTypes from 'prop-types';
import { render } from 'react-dom';

import thunkMiddleware from 'redux-thunk'
// redux, react-router, redux-form, saga, and material-ui
// form the 'kernel' on which admin-on-rest runs
import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createHashHistory';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import createSagaMiddleware from 'redux-saga';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import CustomLayout from './CustomLayout'
import Routes from './Routes'
import CustomMenu from './CustomMenu'

// prebuilt admin-on-rest features
import {
    adminReducer,
    localeReducer,
    crudSaga,
    jsonServerRestClient,
    Delete,
    TranslationProvider,
    declareResources,
} from 'admin-on-rest';

// your app components
import Dashboard from './Dashboard';

import { SiteList, SiteCreate } from './sites';
import SiteEditorContainer from './containers/Publishers/SiteEditorContainer'
import siteEditorReducer from './reducers/Publishers/SiteEditorReducer';

// create a Redux app
const reducer = combineReducers({
    admin: adminReducer,
    locale: localeReducer(),
    form: formReducer,
    routing: routerReducer,
    site: siteEditorReducer
});
const sagaMiddleware = createSagaMiddleware();
const history = createHistory();
const store = createStore(reducer, undefined, compose(
    applyMiddleware(sagaMiddleware, thunkMiddleware, routerMiddleware(history)),
    window.devToolsExtension ? window.devToolsExtension() : f => f,
));
store.dispatch(declareResources([{ name: 'sites' }]));
const restClient = jsonServerRestClient('http://localhost:3000/api/v2/publishers/BuySellAds%20(BSA)');
sagaMiddleware.run(crudSaga(restClient));

// bootstrap redux and the routes
const App = () => (
    <Provider store={store}>
        <TranslationProvider>
            <ConnectedRouter history={history}>
               <CustomLayout customRoutes={Routes} title="TOTO" menu={CustomMenu}/>
                {/* <MuiThemeProvider>
                    <div>
                        <AppBar title="My Admin" />
                        <Switch>
                            <Route exact path="/" component={Dashboard} />
                            <Route exact path="/sites" hasCreate render={(routeProps) => { console.log('routeProps', routeProps); return <SiteList resource="sites" hasCreate   {...routeProps} />} }  />
                            <Route exact path="/sites/create" render={(routeProps) => { console.log('routeProps', routeProps); return <SiteCreate resource="sites" {...routeProps} />} }  />
                            <Route exact path="/sites/:id" hasShow hasDelete render={(routeProps) => <SiteEditorContainer resource="sites" {...routeProps} />} />
                            <Route exact path="/sites/:id/delete" render={(routeProps) => <Delete resource="sites" {...routeProps} />} />
                        </Switch>
                    </div>
                </MuiThemeProvider> */}
            </ConnectedRouter>
        </TranslationProvider>
    </Provider>
);

export default App;
