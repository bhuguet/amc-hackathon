import React from 'react';
import { jsonServerRestClient, Admin, Resource, Delete } from 'admin-on-rest';

// in src/App.js
//import { PostList, PostEdit, PostCreate } from './posts';
//import { UserList } from './users';
import { SiteList, SiteCreate } from './sites';
import SiteEditorContainer from './containers/Publishers/SiteEditorContainer'
import siteEditorReducer from './reducers/Publishers/SiteEditorReducer';
import MyLayout from './CustomLayout';
import CustomAdmin from './CustomAdmin'

//rely on branch feature/ADP-3088-site_collection_unique_functional_id
const App = () => {
    console.log('ENV', process.env)
    return (
        <div>
            <CustomAdmin appLayout={MyLayout} customReducers={{ site: siteEditorReducer }} restClient={jsonServerRestClient(process.env.REACT_APP_SERVER + '/api/v2/publishers/BuySellAds%20(BSA)')}>
                <Resource name="sites" list={SiteList} edit={SiteEditorContainer} create={SiteEditorContainer} remove={Delete} />
            </CustomAdmin>
        </div>
    )
};

export default App;
