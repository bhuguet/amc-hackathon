// in src/Menu.js
import React from 'react';
import { connect } from 'react-redux';
import { MenuItemLink, getResources } from 'admin-on-rest';

const Menu = ({ resources, onMenuTap, logout }) => (
    <div>
        <MenuItemLink to="/" primaryText="Dashboard" onClick={onMenuTap} />
        <MenuItemLink to="/sites" primaryText="Sites" onClick={onMenuTap} />
    </div>
);

export default Menu;
