import FlatButton from 'material-ui/FlatButton';
import React, { Component } from 'react';
import { FieldArray, Field } from 'redux-form';
import PropTypes from 'prop-types';

import ContentCreateIcon from 'material-ui/svg-icons/content/create';
import ActionDeleteIcon from 'material-ui/svg-icons/action/delete';

import {List, ListItem} from 'material-ui/List';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import ContentSend from 'material-ui/svg-icons/content/send';
import Subheader from 'material-ui/Subheader';
import Toggle from 'material-ui/Toggle';
import { required, TextField, NullableBooleanInput, Datagrid } from 'admin-on-rest';
import ExpansionPanel from 'material-expansion-panel';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

export class EmbeddedManyInput extends Component {
    constructor(props) {
        super(props);
        const { source } = props;
    }

    state = {
        open: false,
      };
    
      handleToggle = () => {
        this.setState({
          open: !this.state.open,
        });
      };
    
      handleNestedListToggle = (item) => {
        this.setState({
          open: item.state.open,
        });
      };

    getBidderName = () => {
        return 'MonBidder'
        /*if(available_bidders.length === 0 || bidderId === undefined) {
            return '';
        }
        const bidder = _.find(available_bidders, { 'partner_id': bidderId });
        return bidder.name;*/
    }

    
  

    renderList = ({ fields }) => {
        const { children } = this.props;
        var aIcons = [{
            icon: "delete",
            callback: null,
            additionalParams: null
        }];

        var aButtons = [{
            buttonText: "Save",
            callback: null,
            additionalParams: null
        },
        {
            buttonText: "Cancel",
            callback: null,
            additionalParams: null,
            toggleExpand: true
        }];
                                           
        return (
            <div>
                
                {/*fields.map((site, index) => {
                    const currentSite = fields.get(index);
                    const partner_id = currentSite.bidder_id;
                    const { children } = this.props;
                    console.log(children)
                    return <ExpansionPanel title="Title" expandedTitle="Expanded Title" titleIcon="done_all" actionButtons={aButtons} actionIcons={aIcons} >
                                <div>Example Content</div>
                                {React.Children.map(children, input => {
                                    console.log(input);
                                    return input && (
                                        <div key={input.props.source} className={`aor-input-${input.props.source}`} style={input.props.style}>
                                            <Field {...input.props} name={`${site}.${input.props.source}`} component={input.type} />
                                        </div>
                                    )}
                                )}
                            </ExpansionPanel>
                    }
                )*/}

                {/* <List>
                    {fields.map((site, index) => {
                        const currentSite = fields.get(index);
                        const partner_id = currentSite.bidder_id;
                        return <ListItem key={index} 
                            primaryText={this.getBidderName()}
                            leftIcon={<ContentInbox />}
                            initiallyOpen={true}
                            primaryTogglesNestedList={true}
                            nestedItems={[
                                <ListItem
                                key={1}
                                primaryText="Starred"
                                leftIcon={<ActionGrade />}
                                />,
                                <ListItem
                                key={2}
                                primaryText="Sent Mail"
                                leftIcon={<ContentSend />}
                                disabled={true}
                                nestedItems={[
                                    <ListItem key={1} primaryText="Drafts" leftIcon={<ContentDrafts />} />,
                                ]}
                                />,
                                <ListItem
                                key={3}
                                primaryText="Inbox"
                                leftIcon={<ContentInbox />}
                                open={this.state.open}
                                onNestedListToggle={this.handleNestedListToggle}
                                nestedItems={[
                                    <ListItem key={1} primaryText="Drafts" leftIcon={<ContentDrafts />} />,
                                ]}
                                />,
                            ]}
                            />
                        }
                    )}
                </List> */}
            </div>
        )
    }


    render() {
        const { input, resource, label, source, allowEmpty, basePath, onChange, children, meta } = this.props;

        return (
            <FieldArray name={source} component={this.renderList} />
        )
    }
}

EmbeddedManyInput.propTypes = {
    addField: PropTypes.bool.isRequired,
    allowEmpty: PropTypes.bool.isRequired,
    basePath: PropTypes.string,
    children: PropTypes.node,
    label: PropTypes.string,
    meta: PropTypes.object,
    onChange: PropTypes.func,
    input: PropTypes.object,
    resource: PropTypes.string,
    source: PropTypes.string,
};

EmbeddedManyInput.defaultProps = {
    addField: false,
    allowEmpty: false,
};

export default EmbeddedManyInput;
