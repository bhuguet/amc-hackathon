import React from 'react';
import { Route } from 'react-router-dom';
import SiteEditorContainer from './containers/Publishers/SiteEditorContainer';
import Dashboard from './Dashboard';
import { SiteList, SiteCreate } from './sites'
import { Delete } from 'admin-on-rest';

export default [
    <Route exact path="/" component={Dashboard} />,
    <Route exact path="/sites" hasCreate render={(routeProps) => { console.log('routeProps', routeProps); return <SiteList resource="sites" hasCreate   {...routeProps} />} }  />,
    <Route exact path="/sites/create" render={(routeProps) => { console.log('routeProps', routeProps); return <SiteCreate resource="sites" {...routeProps} />} }  />,
    <Route exact path="/sites/:id" hasShow hasDelete render={(routeProps) => <SiteEditorContainer resource="sites" {...routeProps} />} />,
    <Route exact path="/sites/:id/delete" render={(routeProps) => <Delete resource="sites" {...routeProps} />} />
];

