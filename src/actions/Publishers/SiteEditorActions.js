import { push } from 'react-router-redux'
import fetchData from '../utils/fetch.js'
import { HTTP_CODE }  from '../utils/constant'
import { PUBLISHERS } from './actionTypes';

const BASE_PATH = 'http://localhost:3000/api/v2';

const requestSite = () => ({
  type: PUBLISHERS.SITE_EDITOR.REQUEST_SITE
});

const receiveSite = (json) => {
  let {updated_at, publisher_name, ...site_form} = json;
  return ({
  type: PUBLISHERS.SITE_EDITOR.RECEIVE_SITE,
  site: json,
  site_form: site_form
})};

const requestUpdateSite = () => ({
  type: PUBLISHERS.SITE_EDITOR.REQUEST_UPDATE_SITE
});

const siteUpdatedSuccessfully = json => ({
  type: PUBLISHERS.SITE_EDITOR.SITE_UPDATED_SUCCESSFULLY
});

const siteUpdatedFail = json => ({
  type: PUBLISHERS.SITE_EDITOR.SITE_UPDATED_FAIL
});
const requestCreateSite = () => ({
  type: PUBLISHERS.SITE_EDITOR.REQUEST_CREATE_SITE
});

const siteCreated = () => ({
  type: PUBLISHERS.SITE_EDITOR.SITE_CREATED
});

export const clearSiteStore = () => ({
  type: PUBLISHERS.SITE_EDITOR.CLEAR_SITE
});

const requestAvailableBidders = () => ({
  type: PUBLISHERS.SITE_EDITOR.REQUEST_SITE_AVAILABLE_BIDDERS
});

const receiveAvailableBidders = (json) => ({
  type: PUBLISHERS.SITE_EDITOR.RECEIVE_SITE_AVAILABLE_BIDDERS,
  availablebidders: json
});

export function fetchSite(publisherId, siteId) {
  return dispatch => {
    dispatch(requestSite());

    
    return fetchData(`${BASE_PATH}/publishers/${publisherId}/sites/${siteId}?`, {
      errorMsg: 'Error while fetching site'
    })
    .then(json => dispatch(receiveSite(json[0])));
  }
}

export function fetchSiteAvailableBidders() {
  return dispatch => {
    dispatch(requestAvailableBidders());

    return fetchData(`${BASE_PATH}/all_bidders/`, {
      errorMsg: 'Error while fetching available bidders'
    })
    .then(json => json.map(action => ({
        ...action,
        active: !!action.active,
        id: action.partner_id
    })))
    .then(json => dispatch(receiveAvailableBidders(json)));
  }
}

export function createSite(publisherId, data) {
  return dispatch => {
    dispatch(requestCreateSite());
    return fetchData(`${BASE_PATH}/publishers/${publisherId}/sites/`, {
        method: 'POST',
        body: JSON.stringify({
          element: data
        }),
        errorMsg: 'Error while updating site configuration'
      })
      .then(json => {
        dispatch(siteUpdatedSuccessfully());
        dispatch(push(`/publishers/${publisherId}/sites/${data.name}`))
      })
      .catch(json => dispatch(siteUpdatedFail()))
  }
}

export function updateSite(publisherId, siteId, data) {
  return dispatch => {
    dispatch(requestUpdateSite());

    return fetchData(`${BASE_PATH}/publishers/${publisherId}/sites/${siteId}`, {
        method: 'PUT',
        body: JSON.stringify({
          element: data
        }),
        errorMsg: 'Error while updating site configuration'
      })
      .then(json => dispatch(siteUpdatedSuccessfully()))
      .catch(json => dispatch(siteUpdatedFail()))
  }
}
