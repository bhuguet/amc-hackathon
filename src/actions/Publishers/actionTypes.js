import Enum from 'enum'

export const PUBLISHERS = {
    SITE_EDITOR: new Enum([
        'REQUEST_SITE',
        'RECEIVE_SITE',
        'REQUEST_SITE_AVAILABLE_BIDDERS',
        'RECEIVE_SITE_AVAILABLE_BIDDERS',
        'REQUEST_UPDATE_SITE',
        'SITE_UPDATED',
        'REQUEST_CREATE_SITE',
        'SITE_CREATED',
        'CLEAR_SITE',
        'SITE_UPDATED_SUCCESSFULLY',
        'SITE_UPDATED_FAIL'
    ]),
    AVAILABLE_PUBLISHERS: new Enum([
        'REQUEST',
        'RECEIVE'
    ])
}
