import isomorphicFetch from 'isomorphic-fetch';
import { HTTP_CODE } from './constant';

const fetch = (url, params = {}) => {
  // Each user has its own API_TOKEN. Let's use an hardcoded one for now.
  url += (url.indexOf('?') === -1 ? '?' : '&') + `api_token=af825ed497a020a602b8f432e45cdd7efa2708d1a57b1fe24469e20e625685759c844f577604d023294f`;

  Object.assign(params, {
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
  });

  return isomorphicFetch(url, params)
  .then(response => {
    if (response.status !== HTTP_CODE.OK.valueOf()) {
      const msg = params.errorMsg ? params.errorMsg : `${response.status} error when calling "${url}`;

      console.log(msg);

      const error = new Error(msg);
      error.response = response;
      console.error(error);

      throw error;
    }

    if (params.returnResponse) {
      return Promise.resolve(response);
    } else {
      return response.json();
    }
  });
}

export default fetch;
