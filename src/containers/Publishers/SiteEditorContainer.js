import { connect } from 'react-redux'

import { fetchSite, updateSite, createSite, clearSiteStore, fetchSiteAvailableBidders } from '../../actions/Publishers/SiteEditorActions';
import SiteEditorComponent from '../../views/Publishers/SiteEditorComponent';

export default connect(
    state => {
      return {
          site: state.site
      }
    },
  
    dispatch => {
      console.log("CONTAINER ", this.state)
      return {
        onFetchSite: (...params) => {
           dispatch(clearSiteStore());
           dispatch(fetchSiteAvailableBidders());
           dispatch(fetchSite(...params));
        },
        onUpdateSite: (...params) => dispatch(updateSite(...params)),
        onCreateSite: (...params) => dispatch(createSite(...params)),
        onFetchNewSite: () => {
          dispatch(clearSiteStore())
          dispatch(fetchSiteAvailableBidders());
        }
      }
    }
  )(SiteEditorComponent);
  