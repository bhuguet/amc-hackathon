// in src/users.js
import React from 'react';
import { List, Datagrid, TextField, DateField, BooleanField, Edit, SimpleForm, DisabledInput, TextInput, BooleanInput, EditButton, Create, NullableBooleanInput, DeleteButton } from 'admin-on-rest';

export const PublisherList = (props) => { 
    //console.log(props)
    return (
    <List title="All publishers" {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <DateField source="updated_at" />
        </Datagrid>
    </List>
);}

export const PublisherEdit = (props) => (
    <Edit {...props}>
        <SimpleForm>
            <DisabledInput source="id" />
            <TextInput source="name" />
            <DisabledInput source="updated_at" />
        </SimpleForm>
    </Edit>
);