import { PUBLISHERS } from '../../actions/Publishers/actionTypes';

export const initialState = {
    fetching_site: false,
    site: {},
    site_form: {},
    updated: false,
    updated_successfully: false,
    fetching_all_available_bidders: false,
    available_bidders: []
}

export const siteReducer = (state = initialState, action) => {
    console.log("REDUCER ", action)
    switch (action.type) {
      case PUBLISHERS.SITE_EDITOR.REQUEST_SITE:
        return {
            ...state,
            fetching_site: true
        };
      case PUBLISHERS.SITE_EDITOR.RECEIVE_SITE:
        return {
            ...state,
            fetching_site: false,
            site: action.site,
            site_form: action.site_form
        };
      case PUBLISHERS.SITE_EDITOR.CLEAR_SITE:
        return {
            fetching_site: false,
            site: {},
            site_form: {},
            updated: false,
            updated_successfully: false,
            fetching_all_available_bidders: false,
            available_bidders: []
        }
       case PUBLISHERS.SITE_EDITOR.SITE_UPDATED_SUCCESSFULLY:
        return {
            ...state,
            updated: true,
            updated_successfully: true
        }
      case PUBLISHERS.SITE_EDITOR.SITE_UPDATED_FAIL:
        return {
            ...state,
            updated: true,
            updated_successfully: false
        }
      case PUBLISHERS.SITE_EDITOR.REQUEST_SITE_AVAILABLE_BIDDERS:
        return {
            ...state,
            fetching_all_available_bidders: true
        };
      case PUBLISHERS.SITE_EDITOR.RECEIVE_SITE_AVAILABLE_BIDDERS:
        return {
            ...state,
            fetching_all_available_bidders: false,
            available_bidders: action.availablebidders            
        };
      default:
        return state;
    }
}

export default siteReducer;
