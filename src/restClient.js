import { stringify } from 'query-string';
//import { fetchJson, flattenObject } from 'fetchUtils';
import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    DELETE,
    fetchUtils
} from 'admin-on-rest';;
const { fetchJson, flattenObject } = fetchUtils;
/**
 * Maps admin-on-rest queries to a json-server powered REST API
 *
 * @see https://github.com/typicode/json-server
 * @example
 * GET_LIST     => GET http://my.api.url/posts?_sort=title&_order=ASC&_start=0&_end=24
 * GET_ONE      => GET http://my.api.url/posts/123
 * GET_MANY     => GET http://my.api.url/posts/123, GET http://my.api.url/posts/456, GET http://my.api.url/posts/789
 * UPDATE       => PUT http://my.api.url/posts/123
 * CREATE       => POST http://my.api.url/posts/123
 * DELETE       => DELETE http://my.api.url/posts/123
 */
export default (apiUrl, httpClient = fetchJson) => {
    /**
     * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
     * @param {String} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params The REST request params, depending on the type
     * @returns {Object} { url, options } The HTTP request parameters
     */
    const convertRESTRequestToHTTP = (type, resource, params) => {
        let url = '';
        let resource_path = '';
        const options = {};
        switch (type) {
            case GET_LIST: {
                //const { page, perPage } = params.pagination;
                //const { field, order } = params.sort;
                const query = {
                    ...flattenObject(params.filter),
                    //_sort: field,
                    //_order: order,
                    //_start: (page - 1) * perPage,
                    //_end: page * perPage,
                    api_token: "af825ed497a020a602b8f432e45cdd7efa2708d1a57b1fe24469e20e625685759c844f577604d023294f"
                };

                switch(resource) {
                    case "sites": {
                        console.log(flattenObject(params.filter))
                        //const filter_params = flattenObject(params.filter)
                        const filter_params = {"publisher_name":"BuySellAds%20(BSA)"}
                        console.log(filter_params["toto"])
                        
                        resource_path = `publishers/${filter_params["publisher_name"]}/${resource}`
                        break;
                    }
                    case "publishers": {
                        resource_path = `publishers`
                        break;
                    }
                }
                url = `${apiUrl}/${resource_path}?${stringify(query)}`;
                break;
            }
            case GET_ONE:
                const query = {
                    ...flattenObject(params.filter),
                    api_token: "af825ed497a020a602b8f432e45cdd7efa2708d1a57b1fe24469e20e625685759c844f577604d023294f"
                };
                url = `${apiUrl}/publishers/BuySellAds%20(BSA)/${resource}/${params.id}?${stringify(query)}`;
                break;
            case GET_MANY_REFERENCE: {
                const { page, perPage } = params.pagination;
                const { field, order } = params.sort;
                const query = {
                    ...flattenObject(params.filter),
                    [params.target]: params.id,
                    _sort: field,
                    _order: order,
                    _start: (page - 1) * perPage,
                    _end: page * perPage,
                };
                url = `${apiUrl}/${resource}?${stringify(query)}`;
                break;
            }
            case UPDATE:
                const query2 = {
                    ...flattenObject(params.filter),
                    api_token: "af825ed497a020a602b8f432e45cdd7efa2708d1a57b1fe24469e20e625685759c844f577604d023294f"
                };
                url = `${apiUrl}/publishers/BuySellAds%20(BSA)/${resource}/${params.id}?${stringify(query2)}`;
                options.method = 'PUT';
                options.body = JSON.stringify(params.data);
                break;
            case CREATE:
                const query3 = {
                    ...flattenObject(params.filter),
                    api_token: "af825ed497a020a602b8f432e45cdd7efa2708d1a57b1fe24469e20e625685759c844f577604d023294f"
                };
                url = `${apiUrl}/publishers/BuySellAds%20(BSA)/${resource}?${stringify(query3)}`;
                options.method = 'POST';
                options.body = JSON.stringify(params.data);
                break;
            case DELETE:
                const query4 = {
                    ...flattenObject(params.filter),
                    api_token: "af825ed497a020a602b8f432e45cdd7efa2708d1a57b1fe24469e20e625685759c844f577604d023294f"
                };
                url = `${apiUrl}/publishers/BuySellAds%20(BSA)/${resource}/${params.id}?${stringify(query4)}`;
                options.method = 'DELETE';
                break;
            default:
                throw new Error(`Unsupported fetch action type ${type}`);
        }
        return { url, options };
    };

    /**
     * @param {Object} response HTTP response from fetch()
     * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
     * @param {String} resource Name of the resource to fetch, e.g. 'posts'
     * @param {Object} params The REST request params, depending on the type
     * @returns {Object} REST response
     */
    const convertHTTPResponseToREST = (response, type, resource, params) => {
        const { headers, json } = response;
        switch (type) {
            case GET_LIST:
            case GET_MANY_REFERENCE:
                if (!headers.has('x-total-count')) {
                    throw new Error(
                        'The X-Total-Count header is missing in the HTTP Response. The jsonServer REST client expects responses for lists of resources to contain this header with the total number of results to build the pagination. If you are using CORS, did you declare X-Total-Count in the Access-Control-Expose-Headers header?'
                    );
                }
                return {
                    data: json,
                    total: parseInt(
                        headers
                            .get('x-total-count')
                            .split('/')
                            .pop(),
                        10
                    ),
                };
            case CREATE:
                return { data: { ...params.data, id: json.id } };
            default:
                return { data: json };
        }
    };

    /**
     * @param {string} type Request type, e.g GET_LIST
     * @param {string} resource Resource name, e.g. "posts"
     * @param {Object} payload Request parameters. Depends on the request type
     * @returns {Promise} the Promise for a REST response
     */
    return (type, resource, params) => {
        // json-server doesn't handle WHERE IN requests, so we fallback to calling GET_ONE n times instead
        if (type === GET_MANY) {
            return Promise.all(
                params.ids.map(id => httpClient(`${apiUrl}/${resource}/${id}`))
            ).then(responses => ({
                data: responses.map(response => response.json),
            }));
        }

        const { url, options } = convertRESTRequestToHTTP(
            type,
            resource,
            params
        );
        return httpClient(url, options).then(response =>
            convertHTTPResponseToREST(response, type, resource, params)
        );
    };
};