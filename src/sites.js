// in src/users.js
import React, { Component } from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
// import EmbeddedManyInput from './EmbeddedManyInput'

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';


import { SaveButton, Toolbar, required, maxLength, List, Datagrid, TextField, DateField, BooleanField, Edit, SimpleForm, DisabledInput, TextInput, BooleanInput, EditButton, Create, NullableBooleanInput, DeleteButton } from 'admin-on-rest';

export const SiteList = (props) => { 
    console.log('SiteList', props)
    return (
    <List title="All sites for publisher BuySellAds (BSA)" {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <TextField source="domain" />
            <TextField source="publisher_name" />
            <BooleanField source="active" />
            <BooleanField source="partner" />
            <DateField source="updated_at" />
            <EditButton />
            <DeleteButton />
        </Datagrid>
    </List>
);}


const styles = {
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
    },
    gridList: {
      width: 500,
      height: 450,
      overflowY: 'auto',
    },
  };

const PostCreateToolbar = props => <Toolbar {...props} >
    <SaveButton secondary label="post.action.save_and_show" redirect="show" submitOnEnter={true} />
    <SaveButton label="post.action.save_and_add" redirect={false} submitOnEnter={false} raised={false} />
</Toolbar>;


export class SiteEdit extends Component {
    constructor(props) {
        super(props);
        const { source } = props;
        this.state = {
            expanded: false,
            showCardForm: false
          };
    }

    handleExpandChange = (expanded) => {
        console.log('handleExpandChange')
        this.setState({expanded: expanded});
    };

    handleToggle = (event, toggle) => {
        console.log('handleToggle')
        this.setState({expanded: toggle});
    };

    handleExpand = () => {
        console.log('handleExpand')
        this.setState({expanded: true});
    };

    handleReduce = () => {
        console.log('handleReduce')
        this.setState({expanded: false});
    };

    addBidder = ()  => {
        this.setState({showCardForm:!this.state.showCardForm})
    }

    shouldComponentUpdate() {
        return true;
    }

    render() {
        console.log('SHOW', this.state.showCardForm)

        const card = this.state.showCardForm && (<Card>
        <CardHeader
        title="Without Avatar"
        actAsExpander={true}
        showExpandableButton={true}
        />
        <CardText expandable={true}>
            <NullableBooleanInput label="Active?" source="active" validate={[required]} />
        </CardText>
        </Card>);

        console.log(card)
        return (
                 
                <Edit {...this.props}>
                    
                    <SimpleForm toolbar={<PostCreateToolbar />} redirect="show">
                    <div key={Math.random()} > CARD{ card }FIN </div>
                        <DisabledInput source="id"/>
                        <TextInput source="name" validate={[required, maxLength(25)]} />
                        <TextInput source="domain" />
                        <DisabledInput source="publisher_name" />
                        <NullableBooleanInput label="Active?" source="active" validate={[required]} />
                        <BooleanInput label="Partner?" source="partner" validate={[required]} />
                        <DisabledInput source="updated_at" />
                        <FlatButton label="Action1" onClick={this.addBidder} />
                        
                        {/* <EmbeddedManyInput source="bidder_rtb_settings">
                            <TextInput source="site_id" />
                            <NullableBooleanInput source="active"/>
                        </EmbeddedManyInput> */}
                    </SimpleForm>
                </Edit>
               
            
        )}
    };
;


export const SiteCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput source="domain" />
            <NullableBooleanInput label="Active?" source="active" />
            <NullableBooleanInput label="Partner?" source="partner" />
            <TextInput source="region" />
            <TextInput source="tier" />
        </SimpleForm>
    </Create>
);
