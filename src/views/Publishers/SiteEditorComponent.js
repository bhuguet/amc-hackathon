import React, { Component } from 'react'
import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'

class SiteEditorComponent extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const { onFetchSite, onFetchNewSite, match, creation } = this.props;
        if(!creation) {
            onFetchSite('BuySellAds%20(BSA)', match.params.id);
        } else {
            onFetchNewSite();
        }
    }

    render() {
        const { site, creation, match } = this.props;

        return (
            <div>TOTO</div>
        )
    }
}

export default SiteEditorComponent;
